package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
	"github.com/go-chi/chi"
)

type ActiveWindowInfo struct {
	ID          uint32 `json:"id"`
	Title       string `json:"title"`
	ProgramName string `json:"program_name"`
}

func getActiveWindowTitle(X *xgb.Conn, window xproto.Window) (string, error) {
	netWmNameAtom, err := xproto.InternAtom(X, true, uint16(len("_NET_WM_NAME")), "_NET_WM_NAME").Reply()
	if err != nil {
		return "", err
	}

	reply, err := xproto.GetProperty(X, false, window, netWmNameAtom.Atom, xproto.GetPropertyTypeAny, 0, (1<<32)-1).Reply()
	if err != nil {
		// If we can't get _NET_WM_NAME, fall back to WM_NAME
		wmNameAtom, err := xproto.InternAtom(X, true, uint16(len("WM_NAME")), "WM_NAME").Reply()
		if err != nil {
			return "", err
		}
		reply, err = xproto.GetProperty(X, false, window, wmNameAtom.Atom, xproto.GetPropertyTypeAny, 0, (1<<32)-1).Reply()
		if err != nil {
			return "", err
		}
	}

	// WM_NAME property is where the title is
	title := string(reply.Value)
	return title, nil
}

func getActiveWindow(X *xgb.Conn) (xproto.Window, error) {
	// Get the root window on the default screen
	setup := xproto.Setup(X)
	screen := setup.DefaultScreen(X)
	root := screen.Root

	// _NET_ACTIVE_WINDOW = active window
	activeAtom, err := xproto.InternAtom(X, true, uint16(len("_NET_ACTIVE_WINDOW")), "_NET_ACTIVE_WINDOW").Reply()
	if err != nil {
		return 0, err
	}

	// Get the _NET_ACTIVE_WINDOW property from the root window
	activeReply, err := xproto.GetProperty(X, false, root, activeAtom.Atom, xproto.GetPropertyTypeAny, 0, (1<<32)-1).Reply()
	if err != nil {
		return 0, err
	}

	// The property value is the ID of the active window, can be used to expand functionality
	if len(activeReply.Value) == 4 {
		return xproto.Window(xgb.Get32(activeReply.Value)), nil
	}

	return 0, fmt.Errorf("could not find active window ID")
}

func getProgramName(X *xgb.Conn, window xproto.Window) (string, error) {
	wmClassAtom, err := xproto.InternAtom(X, true, uint16(len("WM_CLASS")), "WM_CLASS").Reply()
	if err != nil {
		return "", err
	}

	reply, err := xproto.GetProperty(X, false, window, wmClassAtom.Atom, xproto.GetPropertyTypeAny, 0, (1<<32)-1).Reply()
	if err != nil {
		return "", err
	}

	// WM_CLASS property is where the program name is __often__ stored
	// The property format is typically "instance\0class\0", juast need the class part
	parts := strings.SplitN(string(reply.Value), "\x00", 3)
	if len(parts) >= 2 {
		return parts[1], nil // parts[1] should be the class name
	}

	return "", fmt.Errorf("could not find program name")
}

func main() {
	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		X, err := xgb.NewConn()
		if err != nil {
			http.Error(w, "Failed to connect to X server", http.StatusInternalServerError)
			return
		}
		defer X.Close()

		activeWindow, err := getActiveWindow(X)
		if err != nil {
			http.Error(w, "Failed to get active window", http.StatusInternalServerError)
			return
		}

		title, err := getActiveWindowTitle(X, activeWindow)
		if err != nil {
			http.Error(w, "Failed to get active window title", http.StatusInternalServerError)
			return
		}
		programName, err := getProgramName(X, activeWindow)
		if err != nil {
			// Handle the error or provide a default value
			programName = "Unknown"
		}
		windowInfo := ActiveWindowInfo{
			ID:          uint32(activeWindow),
			Title:       title,
			ProgramName: programName,
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(windowInfo)
	})

	port := os.Getenv("TCP_PORT")
	useTCP := port != "" || os.Getenv("USE_TCP") == "true"

	if useTCP {
		// Use TCP socket
		if port == "" {
			port = "51230" // Default port
		} else {
			// Optional: Validate the port number
			if _, err := strconv.Atoi(port); err != nil {
				log.Fatalf("Invalid TCP_PORT: %s", port)
			}
		}
		address := "localhost:" + port
		log.Printf("Starting TCP server on %s", address)
		if err := http.ListenAndServe(address, r); err != nil {
			log.Fatal(err)
		}
	} else {
		// curl --unix-socket /tmp/application.sock http://localhost/
		socketPath := "/tmp/application.sock"
		if len(os.Args) > 1 {
			socketPath = os.Args[1]
		}

		// Remove old socket if present
		if err := os.RemoveAll(socketPath); err != nil {
			log.Fatal(err)
		}

		// Listen on Unix socket
		listener, err := net.Listen("unix", socketPath)
		if err != nil {
			log.Fatal(err)
		}
		defer listener.Close()

		log.Printf("Starting Unix socket server on %s", socketPath)
		if err := http.Serve(listener, r); err != nil {
			log.Fatal(err)
		}
	}
}
